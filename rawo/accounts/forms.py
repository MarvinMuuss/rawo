from django import forms
from django.contrib.auth.forms import UserCreationForm, PasswordResetForm
from django.contrib.auth.models import User  # fill in custom user info then save it
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

from rawo_base.models import Profil, PartnerProfil


class MyRegistrationForm(UserCreationForm):
    email = forms.EmailField(required=True)
    isPartner = forms.BooleanField(label=_("Partner"), required=False)

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2', 'isPartner')

    def save(self, commit=True):
        mail = self.cleaned_data['email']
        user = super(MyRegistrationForm, self).save(commit=False)
        user.email = mail
        if commit:
            user.save()
            p = Profil.objects.create(user=user, isPartner=self.cleaned_data['isPartner'])
            if p.isPartner:
                pp = PartnerProfil.objects.create(partner=p)
        return user

    def is_valid(self):
        valid = super(MyRegistrationForm, self).is_valid()
        if not valid:
            return valid
        if User.objects.filter(email=self.cleaned_data['email']).exists():
            self.add_error(None, forms.ValidationError(_("This email already used")))
            return False
        return True

    def check_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(_("The two password fields didn't match."))
        return password2


class EmailValidationOnForgotPassword(PasswordResetForm):
    def clean_email(self):
        email = self.cleaned_data['email']
        if not User.objects.filter(email__iexact=email, is_active=True).exists():
            raise ValidationError(_("Es ist kein User mit dieser Emailadresse hinterlegt!"))
        return email


class ProfilForm(forms.ModelForm):
    class Meta:
        model = Profil
        fields = ['firstname', 'name']


class UserForm(forms.ModelForm):
    class Meta:
        model = Profil
        fields = ['firstname', 'name']


class PartnerForm(forms.ModelForm):
    class Meta:
        model = PartnerProfil
        fields = ['street', 'street_nr', 'city', 'city_code']


class PictureForm(forms.ModelForm):
    class Meta:
        model = Profil
        fields = ['picture']
