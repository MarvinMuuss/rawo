/**
 * Created by marvinmuuss on 18.07.17.
 */
$('#s_status').change(function (e) {
    filter();
});

$('#search').change(function (e) {
    filter();
});

function filter() {
    $.ajax({
        type: "GET",
        url: "/api/partner/search/",
        data: {
            'text': $('#search').val(),
            'status': $('#s_status').val(),
        },
        success: function (data, textStatus, jqXHR) {
            $('#partner-data').html(data)
        },
        dataType: 'html'
    });
}