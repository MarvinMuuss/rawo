/**
 * Created by marvinmuuss on 22.07.17.
 */

function textCounter(field) {
    var len = $(field).val().length;
    var remain = 600 - len;
    $('#leftChars').html(remain);
}

$('#introduction').change(function () {
    $.ajax({
        type: "POST",
        url: "/api/partnerprofil/update/introduction/",
        data: {
            'introduction': $(this).val(),
            'csrfmiddlewaretoken': $("input[name=csrfmiddlewaretoken]").val()
        },
        success: function (data) {
            var result = data.result;
            if (result == "SUCCESS") {
                showMessageSuccess(data.message)
            } else if (result == "ERROR") {
                showMessageError(data.message)
            } else if (result == "WARNING") {
                showMessageWarning(data.message)
            } else {
                showMessageInfo(data.message)
            }
        },
        dataType: 'json'
    });
});

$('#picture').change(function () {
    $('#picform').submit();
});

$('input[id^="experience_"]').change(function () {
    var prio = this.id.split('_')[1];
    $.ajax({
        type: "POST",
        url: "/api/partnerprofil/update/experience/",
        data: {
            'text': $(this).val(),
            'prio': prio,
            'csrfmiddlewaretoken': $("input[name=csrfmiddlewaretoken]").val()
        },
        success: function (data) {
            var result = data.result;
            if (result == "SUCCESS") {
                showMessageSuccess(data.message)
            } else if (result == "ERROR") {
                showMessageError(data.message)
            } else if (result == "WARNING") {
                showMessageWarning(data.message)
            } else {
                showMessageInfo(data.message)
            }
        },
        dataType: 'json'
    });
});

$('input[id^="emphasis_"]').change(function () {
    var prio = this.id.split('_')[1];
    $.ajax({
        type: "POST",
        url: "/api/partnerprofil/update/emphasis/",
        data: {
            'text': $(this).val(),
            'prio': prio,
            'csrfmiddlewaretoken': $("input[name=csrfmiddlewaretoken]").val()
        },
        success: function (data) {
            var result = data.result;
            if (result == "SUCCESS") {
                showMessageSuccess(data.message)
            } else if (result == "ERROR") {
                showMessageError(data.message)
            } else if (result == "WARNING") {
                showMessageWarning(data.message)
            } else {
                showMessageInfo(data.message)
            }
        },
        dataType: 'json'
    });
});