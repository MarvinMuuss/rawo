# coding=utf-8
from django.conf.urls import url
from django.urls import reverse_lazy
from django.views.generic import RedirectView

from . import views

urlpatterns = [
    url(r'^profil/$', views.profil, name='profil'),
    url(r'^me/$', views.myPartnerProfil, name='myPartnerProfil'),
    url(r'^partner/$', views.managePartner, name='partner'),
    url(r'^$', RedirectView.as_view(url=reverse_lazy('profil')), name='dashboard'),
]
