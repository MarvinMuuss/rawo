from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.db.models import Q
from django.shortcuts import render

from rawo_base.models import Profil


@login_required
def managePartner(request):
    if request.user.is_superuser:
        p = Profil.objects.filter_partner().select_related('user', 'profil').prefetch_related('assocRoles',
                                                                                              'assocRoles__role')
        return render(request, "accounts/dashboard/partner/partner.html", {
            "section": "dashboard",
            "dashboard": "partner",
            "partner": p,
            'title': 'RAWO Dashboard'
        })
    else:
        raise PermissionDenied


def searchPartner(request):
    if request.method == "GET":
        status = request.GET.get('status')
        text = request.GET.get('text', '')
        p = Profil.objects.filter_partner().select_related('user', 'profil').prefetch_related('assocRoles',
                                                                                              'assocRoles__role')
        if status == "true":
            p = p.filter_partner_active()
        elif status == "false":
            p = p.filter_partner_unactive()
        if text is not None and text != u"":
            p = p.filter(Q(name__icontains=text) | Q(firstname__icontains=text) | Q(
                profil__city__icontains=text))
        return render(request, 'accounts/dashboard/partner/partner_data.html', {'partner': p})
