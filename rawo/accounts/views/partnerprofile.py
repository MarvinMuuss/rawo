from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.shortcuts import render

from accounts.forms import PictureForm
from rawo_base.models import Profil, ExperienceProfil, EmphasisProfil


def viewPartnerProfil(request, id):
    p = get_object_or_404(
        Profil.objects.filter_partner().select_related('user', 'profil').prefetch_related('experience', 'emphasis'),
        partner_id=id)
    exp = p.experience.all()
    _exp = []
    for i in range(0, 5):
        try:
            _exp.append(exp[i])
        except:
            _exp.append(ExperienceProfil.objects.create(profil=p, prio=i))
    emp = p.emphasis.all()
    _emp = []
    for i in range(0, 5):
        try:
            _emp.append(emp[i])
        except:
            _emp.append(EmphasisProfil.objects.create(profil=p, prio=i))
    return render(request, "partner/profile/PartnerProfil.html", {
        'p': p,
        'experience': _exp,
        'emphasis': _emp,
        'title': 'RAWO'
    })


@login_required
def myPartnerProfil(request):
    p = Profil.objects.select_related('user', 'profil').prefetch_related('experience', 'emphasis').get(
        user=request.user)
    if p.isPartner:
        picForm = PictureForm(request.POST or None, request.FILES or None, instance=p)
        if request.method == "POST":
            if picForm.is_valid():
                picForm.save()

        exp = p.experience.all()
        _exp = []
        for i in range(0, 5):
            try:
                _exp.append(exp[i])
            except:
                _exp.append(ExperienceProfil.objects.create(profil=p, prio=i))
        emp = p.emphasis.all()
        _emp = []
        for i in range(0, 5):
            try:
                _emp.append(emp[i])
            except:
                _emp.append(EmphasisProfil.objects.create(profil=p, prio=i))
        if p.profil.introduction is None:
            intro = 0
        else:
            intro = len(p.profil.introduction)
        return render(request, "accounts/dashboard/profil/partnerprofil.html", {
            'p': p,
            'leftChars': 600 - intro,
            'experience': _exp,
            'emphasis': _emp,
            'pictureForm': picForm,
            'section': 'dashboard',
            'dashboard': 'pp',
            'title': 'RAWO'
        })
    else:
        raise PermissionDenied


def updateIntroduction(request):
    if request.method == "POST":
        if request.is_ajax():
            text = request.POST.get('introduction')
            if len(text) > 600:
                context = {
                    'result': 'ERROR',
                    'message': 'Maximal 600 Zeichen'
                }
                return JsonResponse(context)
            profil = Profil.objects.select_related('user', 'profil').get(user=request.user)
            profil.profil.introduction = text
            profil.profil.save()
            context = {
                'result': 'SUCCESS',
                'message': 'Erfolgreich gespeichert'
            }
            return JsonResponse(context)
    else:
        context = {
            'result': 'ERROR',
            'message': 'Unbekannter Fehler'
        }
        return JsonResponse(context)


def updateExperience(request):
    if request.method == "POST":
        if request.is_ajax():
            prio = request.POST.get('prio')
            text = request.POST.get('text')
            expOtps = ExperienceProfil._meta
            ml = expOtps.get_field('desc').max_length
            if len(text) > ml:
                context = {
                    'result': 'ERROR',
                    'message': 'Der Eintrag ist zu lang'
                }
                return JsonResponse(context)
            profil = Profil.objects.select_related('user', 'profil').prefetch_related('experience').get(
                user=request.user)
            exp = profil.experience.get(prio=prio)
            exp.desc = text
            exp.save()
            context = {
                'result': 'SUCCESS',
                'message': 'Erfolgreich gespeichert'
            }
            return JsonResponse(context)
    else:
        context = {
            'result': 'ERROR',
            'message': 'Unbekannter Fehler'
        }
        return JsonResponse(context)


def updateEmphasis(request):
    if request.method == "POST":
        if request.is_ajax():
            prio = request.POST.get('prio')
            text = request.POST.get('text')
            empOtps = EmphasisProfil._meta
            ml = empOtps.get_field('desc').max_length
            if len(text) > ml:
                context = {
                    'result': 'ERROR',
                    'message': 'Der Eintrag ist zu lang'
                }
                return JsonResponse(context)
            profil = Profil.objects.select_related('user', 'profil').prefetch_related('emphasis').get(
                user=request.user)
            emp = profil.emphasis.get(prio=prio)
            emp.desc = text
            emp.save()
            context = {
                'result': 'SUCCESS',
                'message': 'Erfolgreich gespeichert'
            }
            return JsonResponse(context)
    else:
        context = {
            'result': 'ERROR',
            'message': 'Unbekannter Fehler'
        }
        return JsonResponse(context)
