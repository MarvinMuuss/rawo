import requests
from django.contrib import messages
from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.core.mail import mail_managers
from django.shortcuts import render, redirect
from django.utils.translation import ugettext_lazy as _

from accounts.forms import MyRegistrationForm, PartnerForm, UserForm, ProfilForm


def register(request, *args, **kwargs):
    userCreationForm = MyRegistrationForm(request.POST or None)
    if request.method == "POST" and userCreationForm.is_valid():
        if userCreationForm.check_password2():
            user = userCreationForm.save()
            messages.success(request, _("success"))
            if request.POST.get('isPartner'):
                # Send Email to admins
                mail_managers(subject="Ein neuer Partner hat sich registriert",
                              message="Der User {} / {} hat sich so eben registriert".format(user.username, user.email))
            login(request, user)
            return redirect('dashboard')

    return render(request, "accounts/register.html",
                  {"userCreationForm": userCreationForm})


@login_required
def profil(request):
    user = request.user.profil
    if user.isPartner:
        form = ProfilForm(request.POST or None, instance=user)
        partner = PartnerForm(request.POST or None, instance=user.profil)
    else:
        form = UserForm(request.POST or None, instance=user)
        partner = None
    if request.method == "POST":
        if form.is_valid():
            form.save()
        else:
            pass
        if user.isPartner:
            if partner.is_valid():
                street = partner.cleaned_data['street']
                street_nr = partner.cleaned_data['street_nr']
                city = partner.cleaned_data['city']
                city_code = partner.cleaned_data['city_code']
                r = requests.get(
                    'https://maps.googleapis.com/maps/api/geocode/json?address=' + str(street) + '+' + str(
                        street_nr) + '+' + str(city_code) + '+' + str(
                        city) + '&key=AIzaSyD5bs4Gj3SBfLqn64iNBQBUWaWYbG82qEY')
                json = r.json()
                status = json['status']
                if status == "OK":
                    partner.save()
                    location = json['results'][0]['geometry']['location']
                    user.profil.latitude = location['lat']
                    user.profil.longitude = location['lng']
                    user.profil.save()
                    messages.success(request, _("SUCCESS"))
                elif status == "ZERO_RESULTS":
                    messages.error(request, _("ZERO_RESULTS"))
                elif status == "OVER_QUERY_LIMIT":
                    messages.error(request, _("OVER_QUERY_LIMIT"))
                else:
                    messages.error(request, _("UNKNOWN_ERROR"))

    context = {
        'form': form,
        'partner': partner,
        'dashboard': 'profil',
        'section': 'dashboard',
    }
    return render(request, "accounts/dashboard/profil/profil.html", context)
