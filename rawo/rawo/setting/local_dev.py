from .base import *
DEBUG = False
ALLOWED_HOSTS = ['*']
INSTALLED_APPS += [
    'debug_toolbar',  # and other apps for local development
]
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'theworld',
        'USER': 'rawo',
        'PASSWORD': 'rawo360s_db',
        'HOST': 'theworld.cpklmwvmujr2.eu-central-1.rds.amazonaws.com',
        'PORT': '5432',
    }
}

def show_toolbar(request):
    return bool(settings.DEBUG)


DEBUG_TOOLBAR_CONFIG = {
    'SHOW_TOOLBAR_CALLBACK': show_toolbar,
}

# Local Dev Email
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
DEFAULT_FROM_EMAIL = 'testing@example.com'
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
EMAIL_USE_TLS = False
EMAIL_PORT = 1025
SERVER_EMAIL = "office@raw360.at"
