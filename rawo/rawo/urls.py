"""rawo URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth import views as django_views
from django.views.generic import TemplateView

import accounts.views as accviews
import raworld.views as worldviews

handler403 = 'rawo_base.views.handler403'
handler404 = 'rawo_base.views.handler404'
handler500 = 'rawo_base.views.handler500'

urlpatterns = i18n_patterns(
    url(r'^world/', include('rawo.world_urls')),
    url('^', worldviews.world)
)

urlpatterns = urlpatterns + [
    url(r'^admin/', admin.site.urls),
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url('^', include('raworld.api_urls')),
    url('api/', include('rawo_base.api_urls')),

    # login
    url(r'^register/$', accviews.register, name='register'),
    url(r'^login/$', django_views.LoginView.as_view, name='login'),
    url(r'^logout/$', django_views.LogoutView.as_view, name='logout'),
    # change password


    url(r'^403/$', TemplateView.as_view(template_name='errors/403.html')),
    url(r'^404/$', TemplateView.as_view(template_name='errors/404.html')),
    url(r'^500/$', TemplateView.as_view(template_name='errors/500.html')),
]

if settings.DEBUG is True:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
                      url(r'^__debug__/', include(debug_toolbar.urls)),
                  ] + urlpatterns
