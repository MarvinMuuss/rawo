from django.conf.urls import url, include

urlpatterns = [
    url('^', include('raworld.urls')),
    url('^dashboard/', include('accounts.urls')),
    url('^partner/', include('accounts.partner_urls')),
    url(r'^messages/', include('postman.urls', namespace='postman')),
]