from django.contrib import admin

from .models import *


class RoleInline(admin.TabularInline):
    model = PartnerRoleAssoc
    extra = 1


class ProfilInline(admin.StackedInline):
    model = PartnerProfil


class ExperienceInline(admin.TabularInline):
    model = ExperienceProfil
    extra = 1


class EmphasislInline(admin.TabularInline):
    model = EmphasisProfil
    extra = 1

class ProfilAdmin(admin.ModelAdmin):
    inlines = (ProfilInline, RoleInline, ExperienceInline, EmphasislInline)
    list_display = ('user', 'firstname', 'name')
    list_filter = ('isPartner', 'isActive',)


# Register your models here.
admin.site.register(Profil, ProfilAdmin)
admin.site.register(PartnerProfil)
admin.site.register(Role)
# admin.site.register(PartnerRoleAssoc)
