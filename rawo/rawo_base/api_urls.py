# coding=utf-8
from django.conf.urls import url

from accounts import views as acc

urlpatterns = [
    url(r'^partner/search/', acc.searchPartner, name='partnerSearch'),
    url(r'^partnerprofil/update/introduction/', acc.updateIntroduction),
    url(r'^partnerprofil/update/experience/', acc.updateExperience),
    url(r'^partnerprofil/update/emphasis/', acc.updateEmphasis),
]
