from django.db import models


class ProfileQuerySet(models.QuerySet):
    def filter_partner(self):
        return self.filter(isPartner=True)

    def filter_partner_unactive(self):
        return self.filter(isPartner=True, isActive=False)

    def filter_partner_active(self):
        return self.filter(isPartner=True, isActive=True)

    def filter_roles(self):
        return self.exclude(assocRoles=None)

    def filter_by_roles(self, roles):
        return self.filter(assocRoles__role_id__in=roles)
