# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-07-19 07:13
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rawo_base', '0012_auto_20170718_2131'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='emphasisprofil',
            options={'managed': True, 'ordering': ['prio']},
        ),
        migrations.AlterModelOptions(
            name='experienceprofil',
            options={'managed': True, 'ordering': ['prio']},
        ),
        migrations.AlterUniqueTogether(
            name='emphasisprofil',
            unique_together=set([('profil', 'prio')]),
        ),
        migrations.AlterUniqueTogether(
            name='experienceprofil',
            unique_together=set([('profil', 'prio')]),
        ),
    ]
