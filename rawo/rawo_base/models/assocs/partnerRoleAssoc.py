from django.db import models

from rawo_base.models import Profil, Role

from django.utils.translation import ugettext as _

class PartnerRoleAssoc(models.Model):
    partnerRoleId = models.AutoField(primary_key=True)
    partner = models.ForeignKey(
        Profil, models.CASCADE, blank=False, null=False, related_name="assocRoles")
    role = models.ForeignKey(
        Role, models.CASCADE, blank=False, null=False, related_name="assocPartner")

    def __str__(self):
        return str(self.partner) + " " + _("istEin") + " " + str(self.role)

    class Meta:
        managed = True
        verbose_name = "PartnerRoleAssoc"
        verbose_name_plural = "PartnerRoleAssocs"

