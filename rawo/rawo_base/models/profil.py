# encoding=utf-8
from django.conf import settings
from django.db import models
from django.utils.translation import ugettext as _

from rawo_base.manager.profile import ProfileQuerySet


class Profil(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    partner_id = models.AutoField(primary_key=True)
    firstname = models.CharField(verbose_name=_("Vorname"), max_length=70, null=True, blank=True)
    name = models.CharField(verbose_name=_("Name"), max_length=70, null=True, blank=False)
    picture = models.ImageField(upload_to="userPictures/", blank=True, null=True, verbose_name="Profilbild")
    isPartner = models.BooleanField(_('Partner'), default=False)
    isActive = models.BooleanField(_('aktiviert'), default=False)
    objects = ProfileQuerySet.as_manager()

    def __str__(self):
        return str(self.user.username)

    class Meta:
        managed = True
        verbose_name_plural = 'Profile'


class PartnerProfil(models.Model):
    partner = models.OneToOneField(
        Profil, models.CASCADE, blank=True, null=False, related_name="profil")
    street = models.CharField(verbose_name=_("Straße"), max_length=70, null=True, blank=False)
    street_nr = models.CharField(verbose_name=_("Nummer"), max_length=4, null=True, blank=False)
    city = models.CharField(verbose_name=_("Stadt"), max_length=70, null=True, blank=False)
    city_code = models.CharField(verbose_name=_("PLZ"), max_length=6, null=True, blank=False)
    latitude = models.FloatField(blank=False, null=True)
    longitude = models.FloatField(blank=False, null=True)
    introduction = models.TextField(blank=True, null=True, max_length=600)

    def __str__(self):
        return str(self.partner) + " Profil"

    class Meta:
        managed = True


class ExperienceProfil(models.Model):
    profil = models.ForeignKey(
        Profil, models.CASCADE, blank=True, null=False, related_name="experience")
    desc = models.CharField(verbose_name=_("Beschreibung"), max_length=85, null=True, blank=True)
    prio = models.CharField(verbose_name=_("Prio"), max_length=1, null=True, blank=True)

    def __str__(self):
        return str(self.profil) + " Erfahrungen"

    class Meta:
        managed = True
        ordering = ['prio']
        unique_together = (('profil','prio'))


class EmphasisProfil(models.Model):
    profil = models.ForeignKey(
        Profil, models.CASCADE, blank=True, null=False, related_name="emphasis")
    desc = models.CharField(verbose_name=_("Beschreibung"), max_length=85, null=True, blank=True)
    prio = models.CharField(verbose_name=_("Prio"), max_length=1, null=True, blank=True)

    def __str__(self):
        return str(self.profil) + " fachliche Schwerpunkte"

    class Meta:
        managed = True
        ordering = ['prio']
        unique_together = (('profil','prio'))