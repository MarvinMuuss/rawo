from django.db import models

from django.utils.translation import ugettext as _


class Role(models.Model):
    name = models.CharField(verbose_name=_("Name"), null=False, blank=False, max_length=70)
    picture = models.ImageField(upload_to="rolePictures/", blank=True, null=True, verbose_name='Bild')

    def __str__(self):
        return str(self.name)

    class Meta:
        managed = True
        ordering = ['name']
        verbose_name = 'Rolle'
        verbose_name_plural = 'Rollen'