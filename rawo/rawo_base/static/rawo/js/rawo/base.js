/**
 * Created by marvinmuuss on 07.05.17.
 */
$('a[id^=changeLang_]').click(function(){
    var infos = this.id.split('_');
    $('input[name=language]').val(infos[1]);
    $('#changeLanguage').submit();
});
function showMessageSuccess(text){
    $.notify({
    // options
    message: text
}, {
    // settings
    type: 'success',
    offset: {x:20, y:70},
	mouse_over: 'pause',
	animate: {
		enter: 'animated fadeInDown',
		exit: 'animated fadeOutUp'
	},
    newest_on_top: true
});
}
function showMessageError(text){
    $.notify({
    // options
    message: text
}, {
    // settings
    type: 'danger',
    offset: {x:20, y:70},
	mouse_over: 'pause',
	animate: {
		enter: 'animated fadeInDown',
		exit: 'animated fadeOutUp'
	},
    newest_on_top: true
});
}
function showMessageWarning(text){
    $.notify({
    // options
    message: text
}, {
    // settings
    type: 'warning',
    offset: {x:20, y:70},
	mouse_over: 'pause',
	animate: {
		enter: 'animated fadeInDown',
		exit: 'animated fadeOutUp'
	},
    newest_on_top: true
});
}
function showMessageInfo(text){
    $.notify({
    // options
    message: text
}, {
    // settings
    type: 'info',
    offset: {x:20, y:70},
	mouse_over: 'pause',
	animate: {
		enter: 'animated fadeInDown',
		exit: 'animated fadeOutUp'
	},
    newest_on_top: true
});
}