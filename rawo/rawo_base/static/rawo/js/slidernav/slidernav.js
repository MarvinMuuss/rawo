/*
 *  SliderNav - A Simple Content Slider with a Navigation Bar
 *  Copyright 2015 Monji Dolon, http://mdolon.com/
 *  Released under the MIT, BSD, and GPL Licenses.
 *  More information: http://devgrow.com/slidernav
 */
$.fn.sliderNav = function (options) {
    var defaults = {
        items: ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"],
        debug: false,
        height: null,
        arrows: false,
        event: 'click'
    };
    var opts = $.extend(defaults, options);
    var o = $.meta ? $.extend({}, opts, $$.data()) : opts;
    var slider = $(this);
    $(slider).addClass('slider');
    $('.slider-content li:first', slider).addClass('selected');
    for (var i = 0; i < o.items.length; ++i) $('.slider-nav ul, slider').append("<li><a alt='#" + o.items[i] + "'>" + o.items[i] + "</a></li>");
    $('.slider-nav a', slider).on(opts.event, function (event) {
        event.preventDefault();
        var target = $(this).attr('alt');
        var cOffset = $('.slider-content', slider).offset().top;
        var tOffset = $('.slider-content ' + target, slider).offset().top;
        var height = $('.slider-nav', slider).height();
        if (o.height) height = o.height;
        var pScroll = (tOffset - cOffset) - height / 8;
        $('.slider-content tr', slider).removeClass('selected');
        $(target).addClass('selected');
        $('.slider-content, body').animate({
                scrollTop: pScroll+130
            }, 'slow');
    });
};
