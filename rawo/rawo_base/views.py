from django.shortcuts import render_to_response


def handler403(request, exception):
    response = render_to_response('errors/403.html', {'request': request})
    response.status_code = 403
    return response


def handler404(request, exception):
    response = render_to_response('errors/404.html', {'request': request})
    response.status_code = 404
    return response


def handler500(request):
    response = render_to_response('errors/500.html', {'request': request})
    response.status_code = 500
    return response
