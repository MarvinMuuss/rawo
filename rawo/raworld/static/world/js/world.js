/**
 * Created by marvinmuuss on 15.07.17.
 */

$('#getRoles').click(function (e) {
    e.preventDefault();
    var roles = [];
    $("input[name='role']").each(function () {
        if ($(this).prop('checked')) {
            roles.push($(this).val());
        }
    });

    $.ajax({
        type: 'GET',
        url: "/updatePartner/",
        context: this,
        data: {
            'roles': roles,
        },
        success: function (json) {
            clearOverlays();
            $(json.partner).each(function () {
                var data = this;
                var p = data.partner;
                var pos = data.pos;
                var img = data.pic;

                var marker = new google.maps.Marker({
                    position: pos,
                    map: map,
                    icon: img,
                    animation: google.maps.Animation.DROP,
                });
                markersArray.push(marker);
                marker.addListener('click', function () {
                    infowindow.close();
                    infowindow.setContent(data.infobox);
                    infowindow.open(map, this);
                });
            });
            markerCluster = new MarkerClusterer(map, markersArray,
                {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
        },
        dataType: 'json'
    });
});