import json
import logging

from django.contrib import messages
from django.http import JsonResponse
from django.shortcuts import render, render_to_response

from rawo_base.models import Profil, Role

logger = logging.getLogger(__name__)


def world(request):
    logger.info("ENTER HOME")
    partner = Profil.objects.filter_partner_active().filter_roles().select_related('user', 'profil').prefetch_related(
        'assocRoles',
        'assocRoles__role')
    roles = Role.objects.all()
    _p = buildPartnerContext(partner, roles)
    data = json.dumps(_p)
    context = {
        'section': 'world',
        'title': 'The World',
        'partner': data,
        'roles': roles
    }
    return render(request, 'world/world.html', context)


def updateWorld(request):
    logger.info("UPDATE WORLD")
    roles = request.GET.getlist('roles[]')

    partner = Profil.objects.filter_partner_active().select_related('user', 'profil').prefetch_related(
        'assocRoles',
        'assocRoles__role').filter_by_roles(
        roles).distinct()
    _p = buildPartnerContext(partner, roles)
    context = {
        'partner': _p
    }
    return JsonResponse(context)


def buildPartnerContext(partner, roles):
    _p = []
    for p in partner:
        c = {}
        c['partner'] = str(p.user.username)
        c['pos'] = {'lat': p.profil.latitude, 'lng': p.profil.longitude}
        c['pic'] = p.assocRoles.select_related('role').filter(role_id__in=roles).first().role.picture.url
        c['infobox'] = (
            render_to_response(template_name="world/partnerprofil.html",
                               context={'p': p, 'roles': p.assocRoles.all()}).content).decode("utf-8")
        _p.append(c)
    return _p
